class Apicalypse:
    SORT_ASC = 'asc'
    SORT_DESC = 'desc'

    def __init__(self):
        self._filter = []

    def fields(self, field, *args):
        self._filter.append(f'fields {",".join((field,) + args)}')
        return self

    def sort(self, field, direction=SORT_ASC):
        self._filter.append(f'sort {field} {direction}')
        return self

    def limit(self, value):
        self._filter.append(f'limit {value}')
        return self

    def offset(self, value):
        self._filter.append(f'offset {value}')
        return self

    def search(self, value):
        self._filter.append(f'search "{value}"')
        return self

    def where(self, query, *args):
        self._filter.append(f'where {" & ".join((query,) + args)}')
        return self

    def build(self):
        return '; '.join(self._filter) + ';'
