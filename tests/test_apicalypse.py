import pytest
from python_apicalypse.apicalypse import Apicalypse


@pytest.fixture
def apicalypse():
    return Apicalypse()


def test_fields(apicalypse):
    apicalypse.fields('name')
    assert apicalypse._filter.pop() == 'fields name'


def test_fields_with_multiple_values(apicalypse):
    apicalypse.fields('name', 'id', 'url')
    assert apicalypse._filter.pop() == 'fields name,id,url'


def test_sort(apicalypse):
    apicalypse.sort('rate', Apicalypse.SORT_DESC)
    assert apicalypse._filter.pop() == 'sort rate desc'


def test_sort_default(apicalypse):
    apicalypse.sort('rate')
    assert apicalypse._filter.pop() == 'sort rate asc'


def test_limit(apicalypse):
    apicalypse.limit(30)
    assert apicalypse._filter.pop() == 'limit 30'


def test_offset(apicalypse):
    apicalypse.offset(10)
    assert apicalypse._filter.pop() == 'offset 10'


def test_search(apicalypse):
    apicalypse.search('Uncharted')
    assert apicalypse._filter.pop() == 'search "Uncharted"'


def test_where(apicalypse):
    apicalypse.where('name = "Uncharted"')
    assert apicalypse._filter.pop() == 'where name = "Uncharted"'


def test_where_multiple_values(apicalypse):
    apicalypse.where('name = "Uncharted"', 'id != 48')
    assert apicalypse._filter.pop() == 'where name = "Uncharted" & id != 48'


def test_build(apicalypse):
    apicalypse.fields('id', 'name') \
           .where('name = "witcher"', 'platform != "xbox"') \
           .limit(30) \
           .offset(50) \
           .sort('name') \
           .search("the witcher")
    assert apicalypse.build() == (
            'fields id,name; '
            'where name = "witcher" & platform != "xbox"; '
            'limit 30; '
            'offset 50; '
            'sort name asc; '
            'search "the witcher";')
